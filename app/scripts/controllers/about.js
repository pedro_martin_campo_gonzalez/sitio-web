'use strict';

/**
 * @ngdoc function
 * @name sitioWebApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sitioWebApp
 */
angular.module('sitioWebApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
