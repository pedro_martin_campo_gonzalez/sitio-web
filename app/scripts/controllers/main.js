'use strict';

/**
 * @ngdoc function
 * @name sitioWebApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sitioWebApp
 */
angular.module('sitioWebApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
